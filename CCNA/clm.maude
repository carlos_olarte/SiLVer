*** ****************************************************************
*** CLM: Complete Lattice Monoid
*** ****************************************************************
*** This module implements the algebraic structure needed to specify
*** constraints, i.e., a complete lattice monoid. Such structure is
*** built from:
***   - A carrier ser (ELT from the TRIVIAL theory)
***   - A pre-order (reflexive and transitive relation)
***   - top and bottom elements in the lattic
***   - A commutative and associative operation (tensor).
***   - A residuation operator
***
*** Some instances of this structure are defined in the end of the
*** file including the usual structure to accumulate costs and
*** probabilistic and fuzzy CLMs
*** ****************************************************************

fth CLM is
    protecting BOOL .
    including TRIV .
    pr CONVERSION .
    pr STRING .

    vars X Y Z : Elt .

    --- Order
    op order : Elt Elt -> Bool .
    eq order(X , X) = true [nonexec label reflexive] .
    ceq order(X , Z) = true if order(X , Y) /\ order(Y , Z) [nonexec label transitive] .
      
      --- Bottom and top elements
    ops top bot : -> Elt .
    eq order(X, top) = true [nonexec label top-order] .
    eq order(bot, X) = true [nonexec label bot-order] .

    --- Accumulating elements
    op tensor : Elt Elt -> Elt [comm assoc] .
    eq tensor(X , bot) = bot [nonexec label bot-abs] .
    eq tensor(X , top) = X [nonexec label top-neutro] .
    
    --- Residuation operator
    op div : Elt Elt -> Elt .
    eq order(X, tensor(Y , div( X ,Y )))  = true [nonexec label res-tensor] .

    --- Printing
    op toString : Elt -> String .

endfth

--- CLM with free variables subject to subsitution
fmod CLM-VAR { X :: CLM } is
    --- Adding variables
    pr QID .
    sort XELT .
    subsort X$Elt < XELT .
    op var : Qid -> XELT .

    --- Mappings for substitutions
    sorts MapEntry MEs Map .
    subsort MapEntry < MEs .
    op emptymap : -> MEs .
    op _|->_ : Qid  XELT -> MapEntry [prec 10]  .
    op _;_ : MEs MEs -> MEs [ctor assoc comm id: emptymap] .
    op [[_]] : MEs -> Map .

    op add : MapEntry Map -> Map .
    eq add(Q |-> X , [[ Me ]] ) = [[ Me ;  Q |-> X ]] .

    --- CLM operators at the level of XELM 
    op Tensor : XELT XELT -> XELT [assoc comm] .
    op Div : XELT XELT -> XELT [assoc comm] .

    vars x y : X$Elt .
    vars X Y : XELT .
    vars Me Me' : MEs .
    var M : Map .
    var Q : Qid .

    --- Tensor and Div only totally defined for ground terms
    eq Tensor(x,y) = tensor(x,y) .
    eq Div(x,y) = div(x,y) .
    
    --- Subsitution: it is assumed that the MAP maps all the variable
    --- to concrete values
    op subs : XELT Map -> X$Elt .

    --- It is assumed that y is an "instantiated" value. 
    eq subs(var(Q), [[ Q |-> Y ; Me ]] ) = Y .

    --- Expressions may include tensor(var('x) , 3.0)
    eq subs(Tensor(X,Y), M) = Tensor(subs(X, M), subs(Y, M)) .
    eq subs(Div(X,Y), M) = Div(subs(X, M), subs(Y, M)) .
    eq subs(x, M) = x .

    
    --- Substitution at the level of X$Elt
    op subs : Map Map -> Map .
    eq subs( [[ Q |-> X ; Me ]] , M ) = add( (Q |-> subs(X,M)) , subs( [[ Me ]], M ) ) .
    eq subs( [[ emptymap ]] , M) = [[ emptymap ]] .
    


endfm
    
--- A CLM to specify costs. The ordering on the real numbers is the
--- inverse of <= (0 is top and inf is the bottom)
fmod FLOAT-CLM is
    including FLOAT .
    pr CONVERSION .
    sort Float-Inf .
    subsort Float < Float-Inf .
    op inf : -> Float-Inf [ctor]  . *** adding bot to the lattice
    op order : Float-Inf Float-Inf -> Bool .
    vars X Y Z : Float .

    op top : -> Float-Inf .
    eq top = 0.0 .
    op bot : -> Float-Inf .
    eq bot = inf .

    eq order(inf, X) = true .
    eq order(inf, inf) = true .
    eq order(X, inf) = false .
    eq order(X,Y) = X >= Y .

    op tensor : Float-Inf Float-Inf -> Float-Inf [assoc comm] .
    eq tensor(inf, inf) = inf .
    eq tensor(inf, X) = inf .
    eq tensor(X,Y) = X + Y .

    op div : Float-Inf Float-Inf -> Float-Inf .
    eq div(inf, inf) = inf .
    eq div(inf, X) = inf .
    eq div(X, inf) = inf .
    eq div(X,Y) = if X >= Y then X - Y else top fi .

    op toString : Float-Inf -> String .
    eq toString(X) = string(X) .
    eq toString(inf) = "inf" .
    
endfm

view Float-CLM from CLM to FLOAT-CLM is
    sort Elt to Float-Inf .
    op top to top .
    op bot to bot .
    op tensor to tensor .
    op div to div .
    op toString to toString .
endv
    

--- A CLM to specify costs. The ordering on the natural numbers is the
--- inverse of <= (0 is top and inf is the bottom)
fmod NAT-CLM is
    including NAT .
    pr CONVERSION .
    sort Nat-Inf .
    subsort Nat < Nat-Inf .
    op inf : -> Nat-Inf [ctor]  . *** adding bot to the lattice
    op order : Nat-Inf Nat-Inf -> Bool .
    vars X Y Z : Nat .

    op top : -> Nat-Inf .
    eq top = 0 .
    op bot : -> Nat-Inf .
    eq bot = inf .

    eq order(inf, X) = true .
    eq order(inf, inf) = true .
    eq order(X, inf) = false .
    eq order(X,Y) = X >= Y .

    op tensor : Nat-Inf Nat-Inf -> Nat-Inf [assoc comm] .
    eq tensor(inf, inf) = inf .
    eq tensor(inf, X) = inf .
    eq tensor(X,Y) = X + Y .

    op div : Nat-Inf Nat-Inf -> Nat-Inf .
    eq div(inf, inf) = inf .
    eq div(inf, X) = inf .
    eq div(X, inf) = inf .
    eq div(X,Y) = if X >= Y then X - Y else top fi .

    op toString : Nat-Inf -> String .
    eq toString(X) = string(X , 10) .
    eq toString(inf) = "inf" .
    
endfm

view Nat-CLM from CLM to NAT-CLM is
    sort Elt to Nat-Inf .
    op top to top .
    op bot to bot .
    op tensor to tensor .
    op div to div .
    op toString to toString .
endv

--- A CLM specifying fuzzy behaviors. The carrier set is the
--- interval [0,1] and values are accumulated via min. The order is the
--- usual one on reals/floats. 
fmod FUZZY-CLM is
    including FLOAT .
    pr CONVERSION .
    
    op top : -> Float .
    eq top = 1.0 .
    op bot : -> Float .
    eq bot = 0.0 .

    vars X Y : Float .

    op order : Float Float -> Bool .
    eq order(X,Y) = X <= Y .

    op tensor : Float  Float -> Float [assoc comm] .
    eq tensor(X,Y) = min(X , Y) .

    op div : Float Float -> Float .
    eq div(X,Y) = if X <= Y then min(X,Y) else 1.0 fi .

    op toString : Float -> String .
    eq toString(X) = string(X) .
    
endfm

view Fuzzy-CLM from CLM to FUZZY-CLM is
    sort Elt to Float .
    op top to top .
    op bot to bot .
    op tensor to tensor .
    op div to div .
    op toString to toString .
endv


--- A CLM specifying probabilisitic behaviors. The carrier set is the
--- interval [0,1] and values are accumulated via *. The order is the
--- usual one on reals/floats. 
fmod PROB-CLM is
    including FLOAT .
    pr CONVERSION .
    
    op top : -> Float .
    eq top = 1.0 .
    op bot : -> Float .
    eq bot = 0.0 .

    vars X Y : Float .

    op order : Float Float -> Bool .
    eq order(X,Y) = X <= Y .

    op tensor : Float  Float -> Float [assoc comm] .
    eq tensor(X,Y) = X * Y .

    op div : Float Float -> Float .
    eq div(X,Y) = if X <= Y then X / Y else 1.0 fi .

    op toString : Float -> String .
    eq toString(X) = string(X) .
    
endfm

view Prob-CLM from CLM to PROB-CLM is
    sort Elt to Float .
    op top to top .
    op bot to bot .
    op tensor to tensor .
    op div to div .
    op toString to toString .
endv 