# Constrained Core Network Algebra (CCNA)


CCNA extends CNA with constraints  that declaratively allow the modeler to restrict the interaction that should actually happen. More precisely, the CNA model is extended with constraints built from an algebraic structure (a Complete Lattice Monoid --CLM--) that models restrictions that appear naturally in the modeled system at hand. CCNA processes not only offer links but also CLM values and check if constraints on those values are satisfied. 

This repository follow closely the structure of the one for CNA  but all the modules are parametric on the type of constraints that can be used. The entry point is the file ```clm.maude```.

## Complete Lattice Monoids
File ```clm.maude```.  defines the following functional theory (that all CLM should satisfy):

```
fth CLM is
  --- The carrier set
  sort Elt . --- Imported from the theory TRIV
  --- A reflexive and transitive relation
  op order : Elt Elt -> Bool .
  --- Top and bottom element of the lattice
  ops top bot : -> Elt .
  --- Operator to accumulate values
  --- Such operator must be commutative, associative and top must be its unit (x tensor top = x)
  op tensor : Elt Elt -> Elt .
  --- Residuation operator
  --- satisfying x <= tensor(y, div(x,y))
  op div : Elt Elt -> Elt .
  
endfm
```

Examples of those structures are (defined in the same file):
 - ```FLOAT-CLM```. The carrier is the set of non-negative floating point numbers completed with infinity (the bottom of the lattice). The top of the lattice is 0 and the tensor operator is ```+``` on the float numbers. This structure represents "costs" where higher the number the worse. 
- ```NAT-CLM ```. Similar to the previous one but using Maude ```Nat``` numbers. 
-  ```FUZZY-CLM```. Values are taken from the real interval [0,1] and the tensor operation is ```min```.
- ```PROB-CLM```. Values are taken from the real interval [0,1] and the tensor operation is ```*```.


## Constraints
File ```constraint.maude``` defines the kind of constraints supported, namely:

```
 X at<= Y : using the order <= in the CLM
 X at< Y : the strict version of <=
 X at<= Y : (meaning Y <= X)
 X at> Y : the strict version of >=
 X at= Y : meaning X == Y
 X at!= Y : meaning X =/= Y
```

The always true constraint is denoted as ```TT``` and constraints are joined with conjunction (```_/\_```). This file also defines the needed machinery to perform substitutions on free variables. 

## Links
Building links is similar to the case of CNA. 

## Processes
The main difference is in the definition of prefixes that, besides solid links, includes the following cases:
```
--- Prefix with value
 op {!_}_ : X$Elt SLink  -> Prefix .

--- Prefix with constraints
 op {?_}_ : Constraint SLink  -> Prefix .

 --- Prefix with both values and constraints
 op { !_ , ?_ }_ : X$Elt Constraint SLink  -> Prefix .
```

Also, calls to procedure may include substitutions on data variables. For instance, 
```
{ 'A ,  [[ 'v1 |-> 1 ; 'v2 |-> 2 ]] }
```

The __accumulated value variable__ is denoted as ```var('acc)```.

## Configurations
Symbolic configurations are extended to include also the accumulated (CLM) value and the associated constraints. Validity of constraints is updated accordingly to check constraints. 

## Semantics
This file adapts the procedures defined for CNA. 
__Note__: In order to use all the meta-level procedures on modules parametric to a CLM, we had to hirewire the name os such module as ```PROC-INS``` (see file examples.maude).

## Examples
This files tests most of the funcionalities defined here as well as different versions of the the dinning philosopher problem. There are several sections in that file:
```
-----------------------------------------
--- Table of Contents
-----------------------------------------
--- 1. One-step transitions
--- 2. Traces
--- 3. Label transition system and graphs
--- 4. Alternating DP
--- 5. Communicating DP
--- 6. Failing DP
-----------------------------------------
```
